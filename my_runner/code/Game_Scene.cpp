/*
 * GAME SCENE
 * Copyright © 2019+ Miguel Fernández Tormos
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 */

#include "Game_Scene.hpp"

#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>

using namespace basics;
using namespace std;

namespace example
{
    // ---------------------------------------------------------------------------------------------
    // ID y ruta de las texturas que se deben cargar para esta escena. La textura con el mensaje de
    // carga está la primera para poder dibujarla cuanto antes:

    Game_Scene::Texture_Data Game_Scene::textures_data[] =
    {
        { ID(loading),    "game-scene/loading.png"              },
        { ID(player),     "game-scene/player.png"               },
        { ID(divisor),     "game-scene/road_divisor.png"        },
        { ID(obstacle),     "game-scene/obstacle.png"           },
        { ID(background),     "game-scene/bkg.png"              },
        { ID(pause_bt),     "game-scene/pause_bt.png"           },
        { ID(resume_bt),     "game-scene/continue_bt.png"       },
        { ID(restart_bt),     "game-scene/restart_bt.png"       },
        { ID(help_bt),     "game-scene/help_bt.png"             },
        { ID(instructions_bt),"game-scene/instructions_bt.png"  },
    };

    // Pâra determinar el número de items en el array textures_data, se divide el tamaño en bytes
    // del array completo entre el tamaño en bytes de un item:

    unsigned Game_Scene::textures_count = sizeof(textures_data) / sizeof(Texture_Data);

    // ---------------------------------------------------------------------------------------------

    Game_Scene::Game_Scene()
    {
        // Se establece la resolución virtual (independiente de la resolución virtual del dispositivo).
        // En este caso no se hace ajuste de aspect ratio, por lo que puede haber distorsión cuando
        // el aspect ratio real de la pantalla del dispositivo es distinto.

        canvas_width  = 720;
        canvas_height = 1280;

        // Se inicia la semilla del generador de números aleatorios:

        srand (unsigned(time(nullptr)));

        // Se inicializan otros atributos:

        initialize ();
    }

    // ---------------------------------------------------------------------------------------------
    // Algunos atributos se inicializan en este método en lugar de hacerlo en el constructor porque
    // este método puede ser llamado más veces para restablecer el estado de la escena y el constructor
    // solo se invoca una vez.

    bool Game_Scene::initialize ()
    {
        state     = LOADING;
        suspended = true;
        gameplay  = UNINITIALIZED;



        follow_target = false;

        return true;
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::suspend ()
    {
        suspended = true;               // Se marca que la escena ha pasado a primer plano
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::resume ()
    {
        suspended = false;              // Se marca que la escena ha pasado a segundo plano
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::handle (Event & event)
    {
        if (state == RUNNING)               // Se descartan los eventos cuando la escena está LOADING
        {
            if (gameplay == WAITING_TO_START)
            {
                //start_playing ();           // Se empieza a jugar cuando el usuario toca la pantalla por primera vez
            }
            else switch (event.id)
            {
                case ID(touch-started):     // El usuario toca la pantalla
                {
                    user_target_x = *event[ID(x)].as<var::Float>();
                    user_target_y = *event[ID(y)].as<var::Float>();
                    follow_target = true;
                    break;
                }
                case ID(touch-moved):
                case ID(touch-ended):       // El usuario deja de tocar la pantalla
                {
                    follow_target = false;
                    break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::update (float time)
    {
        if (!suspended) switch (state)
        {
            case LOADING: load_textures  ();     break;
            case RUNNING: run_simulation (time); break;
            case ERROR:   break;
        }
    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render (Context & context)
    {
        if (!suspended)
        {
            // El canvas se puede haber creado previamente, en cuyo caso solo hay que pedirlo:

            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            // Si no se ha creado previamente, hay que crearlo una vez:

            if (!canvas)
            {
                 canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            // Si el canvas se ha podido obtener o crear, se puede dibujar con él:

            if (canvas)
            {
                canvas->clear ();

                switch (state)
                {
                    case LOADING: render_loading   (*canvas); break;
                    case RUNNING: render_playfield (*canvas); break;
                    case ERROR:   break;
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    // En este método solo se carga una textura por fotograma para poder pausar la carga si el
    // juego pasa a segundo plano inesperadamente. Otro aspecto interesante es que la carga no
    // comienza hasta que la escena se inicia para así tener la posibilidad de mostrar al usuario
    // que la carga está en curso en lugar de tener una pantalla en negro que no responde durante
    // un tiempo.

    void Game_Scene::load_textures ()
    {
        if (textures.size () < textures_count)          // Si quedan texturas por cargar...
        {
            // Las texturas se cargan y se suben al contexto gráfico, por lo que es necesario disponer
            // de uno:

            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                // Se carga la siguiente textura (textures.size() indica cuántas llevamos cargadas):

                Texture_Data   & texture_data = textures_data[textures.size ()];
                Texture_Handle & texture      = textures[texture_data.id] = Texture_2D::create (texture_data.id, context, texture_data.path);

                // Se comprueba si la textura se ha podido cargar correctamente:

                if (texture) context->add (texture); else state = ERROR;

                // Cuando se han terminado de cargar todas las texturas se pueden crear los sprites que
                // las usarán e iniciar el juego:
            }
        }
        else
        if (timer.get_elapsed_seconds () > 1.5f)        // Si las texturas se han cargado muy rápido
        {                                               // se espera un segundo desde el inicio de
            create_sprites ();                          // la carga antes de pasar al juego para que
                                                        // el mensaje de carga no aparezca y desaparezca
                                                        // demasiado rápido.
            state = RUNNING;
        }
    }

    // ---------------------------------------------------------------------------------------------
    // En este método se crean los Sprites y se les da un valor inicial.

    void Game_Scene::create_sprites ()
    {

        //Nuevos Sprites

        Sprite_Handle player_handle(new Sprite( textures[ID(player)].get() ));

        Sprite_Handle divisor_handle_1(new Sprite( textures[ID(divisor)].get() ));
        Sprite_Handle divisor_handle_2(new Sprite( textures[ID(divisor)].get() ));
        Sprite_Handle divisor_handle_3(new Sprite( textures[ID(divisor)].get() ));
        Sprite_Handle divisor_handle_4(new Sprite( textures[ID(divisor)].get() ));
        Sprite_Handle background_handle(new Sprite( textures[ID(background)].get() ));
        Sprite_Handle pause_button_handle(new Sprite( textures[ID(pause_bt)].get() ));
        Sprite_Handle resume_button_handle(new Sprite( textures[ID(resume_bt)].get() ));
        Sprite_Handle restart_button_handle(new Sprite( textures[ID(restart_bt)].get() ));
        Sprite_Handle help_button_handle(new Sprite( textures[ID(help_bt)].get() ));
        Sprite_Handle instructions_button_handle(new Sprite( textures[ID(instructions_bt)].get() ));


        sprites.push_back (background_handle);
        sprites.push_back (player_handle);
        sprites.push_back (divisor_handle_1);
        sprites.push_back (divisor_handle_2);
        sprites.push_back (divisor_handle_3);
        sprites.push_back (divisor_handle_4);

        bkg             =   background_handle.get();

        player          =      player_handle.get();

        divisor_1       =   divisor_handle_1.get();
        divisor_2       =   divisor_handle_2.get();
        divisor_3       =   divisor_handle_3.get();
        divisor_4       =   divisor_handle_4.get();

        pause_button                = pause_button_handle.get();
        resume_button               = resume_button_handle.get();
        restart_button              = restart_button_handle.get();
        help_button                 = help_button_handle.get();
        instructions_button         = instructions_button_handle.get();


        bkg->set_position({0.f, 0.f});
        bkg->set_anchor (BOTTOM | LEFT);

        divisor_1->set_position ({0.f, 0.f});
        divisor_1->set_anchor (BOTTOM | CENTER);
        divisor_1->set_speed    ({ 0.f, 0.f });

        divisor_2->set_position ({canvas_width / 3.f, 0.f});
        divisor_2->set_anchor (BOTTOM | CENTER);
        divisor_2->set_speed    ({ 0.f, 0.f });

        divisor_3->set_position ({canvas_width * 2.f / 3.f, 0.f});
        divisor_3->set_anchor (BOTTOM | CENTER);
        divisor_3->set_speed    ({ 0.f, 0.f });

        divisor_4->set_position ({canvas_width, 0.f});
        divisor_4->set_anchor (BOTTOM | CENTER);
        divisor_4->set_speed    ({ 0.f, 0.f });

        player->set_position ({canvas_width / 2.f, canvas_height / 3.f});
        player->set_anchor (CENTER);
        player->set_speed    ({ 0.f, 0.f });

        //Esta decalaración es especial porque hay que recorrer el array de obstáculos
        //e ir creándolos uno a uno. Todos empiezan ocultos y en el mismo sitio.

        for(auto & obstacle : obstacles){
            Sprite_Handle obstacle_handle(new Sprite (textures[ID(obstacle)].get() ));
            obstacle = obstacle_handle.get();
            sprites.push_back (obstacle_handle);
            obstacle->hide();
            obstacle->set_anchor(CENTER);
            obstacle->set_speed({0.f, 0.f});
        }

        sprites.push_back (pause_button_handle);
        sprites.push_back (resume_button_handle);
        sprites.push_back (restart_button_handle);
        sprites.push_back (help_button_handle);
        sprites.push_back (instructions_button_handle);

        pause_button->set_anchor(BOTTOM | LEFT);
        pause_button->set_position ({0.f, canvas_height - 100.f});

        resume_button->set_anchor(BOTTOM | LEFT);
        resume_button->set_position ({0.f, canvas_height - 100.f});
        resume_button->hide();

        restart_button->set_anchor(TOP | LEFT);
        restart_button->set_position ({0.f, canvas_height - 100.f});
        restart_button->hide();

        help_button->set_anchor(TOP | LEFT);
        help_button->set_position ({canvas_width / 2.f, canvas_height - 100.f});
        help_button->hide();

        instructions_button->set_anchor(TOP | LEFT);
        instructions_button->set_position ({0.f, canvas_height - 234.f});
        instructions_button->hide();

        //Inicialización de otros valores al empezar la partida.

        obstacle_speed = -300.f;
        generate_enemy_timer = 1.5f;
        follow_target = false;
        pause = false;

        gameplay = PLAYING;

    }

    // ---------------------------------------------------------------------------------------------
    // Cuando el juego se reinicia porque un jugador pierde, se
    // llama a este método para restablecer los valores del player, ocultar los arbustos,
    // y la interfaz de la pantalla del menú de pausa.

    void Game_Scene::restart_game()
    {
        player->set_position ({canvas_width / 2.f, canvas_height / 3.f});

        for(auto & obstacle : obstacles){
            obstacle->hide();
            obstacle->set_speed({0.f, 0.f});
        }

        restart_button->hide();
        pause_button->show();
        instructions_button->hide();
        help_button->hide();
        resume_button->hide();

        obstacle_speed = -300.f;
        generate_enemy_timer = 1.5f;
        follow_target = false;
        pause = false;

        gameplay = PLAYING;
    }


    // ---------------------------------------------------------------------------------------------

    void Game_Scene::run_simulation (float time)
    {

        //Si el juego no está pausado

        if(!pause){

            // Se actualiza el estado de todos los sprites
            for (auto & sprite : sprites)
            {
                sprite->update (time);
            }
            // Se genera un obstáculo en función de un contador que va disminuyendo
            if(obstacle_timer.get_elapsed_seconds () > generate_enemy_timer){
                generate_obstacle();
                obstacle_timer.reset();
            }
            // Se comprueba si un obstáculo ha abandonado la pantalla
            check_obstacle_in_screen();
            // Se comprueba el input del usuario para mover el coche
            update_user ();
            // Se comprueban las colisiones del player con arbustos
            check_player_collisions();
        }
        //Si el juego está pausado, se accede al menú de pausa
        else
        {
            if(follow_target){
                //Si se toca el botón de Resume, se vuelve al juego
                if(user_target_y >= canvas_height - 100.f){
                    pause = false;
                    restart_button->hide();
                    resume_button->hide();
                    help_button->hide();
                    pause_button->show();
                    instructions_button->hide();
                    follow_target = false;


                } else if(user_target_y >= canvas_height - 260.f){
                    //El botón de Restart llama a la función restart_game para reestablecer valores inciales.
                    if(user_target_x <= canvas_width / 2.f) {
                        gameplay = WAITING_TO_START;
                        restart_game();
                    }
                    //El botón Help despliega la explicación del juego o la oculta si está mostrándose
                    else {
                        if(instructions_button->is_visible()){
                            instructions_button->hide();
                            follow_target = false;
                        }

                        else{
                            instructions_button->show();
                            follow_target = false;
                        }

                    }
                }
            }


        }


    }

    // ---------------------------------------------------------------------------------------------
    // Se hace que el player cambie de carril en función de su posición y la pulsación

    void Game_Scene::update_user ()
    {

            if(follow_target){

                if(user_target_y < canvas_height - 100.f){
                    if(user_target_x >= canvas_width / 2.f){                                            //Pulsa a la derecha de la pantalla
                        if(player->get_position_x() <= canvas_width / 3.f){                             //Estoy en el carril izquierdo
                            player->set_position_x(canvas_width / 2.f);
                            follow_target = false;

                        }  else if(player->get_position_x() <= canvas_width / 3.f * 2.f){                //Estoy en el carril central
                            player->set_position_x(canvas_width / 6.f * 5.f);
                            follow_target = false;
                        }
                    }  else {
                                                                                                        //Pulsa a la izquierda de la pantalla
                        if (player->get_position_x() >= canvas_width / 3.f * 2.f) {                      //Estoy en el carril derecho
                            player->set_position_x(canvas_width / 2.f);
                            follow_target = false;

                        } else if (player->get_position_x() >= canvas_width / 3.f) {                     //Estoy en el carril central
                            player->set_position_x(canvas_width / 6.f);
                            follow_target = false;
                        }
                    }

                    //Si no está desplegado el menú de pausa y pulso sobre Pause, se abre el menú
                    //y se detiene el update gracias al booleano pause.
                } else {
                    pause = true;
                    resume_button->show();
                    restart_button->show();
                    help_button->show();
                    pause_button->hide();
                    follow_target = false;
                }


            }


    }

    // ---------------------------------------------------------------------------------------------

    void Game_Scene::render_loading (Canvas & canvas)
    {
        Texture_2D * loading_texture = textures[ID(loading)].get ();

        if (loading_texture)
        {
            canvas.fill_rectangle
            (
                { canvas_width * .5f, canvas_height * .5f },
                { loading_texture->get_width (), loading_texture->get_height () },
                  loading_texture
            );
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Simplemente se dibujan todos los sprites que conforman la escena.

    void Game_Scene::render_playfield (Canvas & canvas)
    {
        for (auto & sprite : sprites)
        {
            sprite->render (canvas);
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Generación de obstáculos
    // Se elige aleatoriamente por qué carril va a salir el obstáculo de los tres.
    // Coge el primer obstáculo no visible y resetea su posición, le da velocidad y lo hace visible.
    // Por último, baja el contador de generación de obstáculos y le aumenta la velocidad.

    void Game_Scene::generate_obstacle(){

        float road_x;

        switch ((rand() % (3))){
            case 0:
                road_x = canvas_width / 6.f;
                break;
            case 1:
                road_x = canvas_width / 2.f;
                break;
            case 2:
                road_x = canvas_width / 6.f * 5.f;
                break;
        }

        for (auto & obstacle : obstacles){

            if(obstacle->is_not_visible()){

                obstacle->show();
                obstacle->set_position({road_x, canvas_height});


                obstacle->set_speed({0.f, obstacle_speed});


                obstacle_speed -= 12.f;

                if(generate_enemy_timer > 0.5f){
                    generate_enemy_timer -= 0.02f;
                }

                break;
            }

        }
    }

    // ---------------------------------------------------------------------------------------------
    // Se comprueba si el obstáculo abandona la pantalla y lo hace invisible para reutilizarlo
    // más adelante.

    void Game_Scene::check_obstacle_in_screen(){

        for (auto & obstacle : obstacles){

            if(obstacle->is_visible()){

                if(obstacle->get_top_y() < 0.f){

                    obstacle->hide();


                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Se comprueba si cualquier obstáculo visible colisiona con el jugador. Se reinicia el juego
    // porque ha perdido el usuario.

    void Game_Scene::check_player_collisions() {

        for(auto & obstacle : obstacles){

            if(obstacle->is_visible()){
                if(player->intersects(*obstacle)){
                    gameplay = WAITING_TO_START;
                    restart_game();
                }
            }



        }

    }

}
