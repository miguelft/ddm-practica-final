/*
 * GAME SCENE
 * Copyright © 2019+ Miguel Fernández Tormos
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 */


#ifndef GAME_SCENE_HEADER
#define GAME_SCENE_HEADER

    #include <map>
    #include <list>
    #include <memory>

    #include <basics/Canvas>
    #include <basics/Id>
    #include <basics/Scene>
    #include <basics/Texture_2D>
    #include <basics/Timer>

    #include "Sprite.hpp"

    namespace example
    {

        using basics::Id;
        using basics::Timer;
        using basics::Canvas;
        using basics::Texture_2D;

        class Game_Scene : public basics::Scene
        {

            // Estos typedefs pueden ayudar a hacer el código más compacto y claro:

            typedef std::shared_ptr < Sprite     >     Sprite_Handle;
            typedef std::list< Sprite_Handle     >     Sprite_List;
            typedef std::shared_ptr< Texture_2D  >     Texture_Handle;
            typedef std::map< Id, Texture_Handle >     Texture_Map;
            typedef basics::Graphics_Context::Accessor Context;

            /**
             * Representa el estado de la escena en su conjunto.
             */
            enum State
            {
                LOADING,
                RUNNING,
                ERROR
            };

            /**
             * Representa el estado del juego cuando el estado de la escena es RUNNING.
             */
            enum Gameplay_State
            {
                UNINITIALIZED,
                WAITING_TO_START,
                PLAYING,
                BALL_LEAVING,
            };

        private:

            /**
             * Array de estructuras con la información de las texturas (Id y ruta) que hay que cargar.
             */
            static struct   Texture_Data { Id id; const char * path; } textures_data[];

            /**
             * Número de items que hay en el array textures_data.
             */
            static unsigned textures_count;


        private:

            State          state;                               ///< Estado de la escena.
            Gameplay_State gameplay;                            ///< Estado del juego cuando la escena está RUNNING.
            bool           suspended;                           ///< true cuando la escena está en segundo plano y viceversa.

            unsigned       canvas_width;                        ///< Ancho de la resolución virtual usada para dibujar.
            unsigned       canvas_height;                       ///< Alto  de la resolución virtual usada para dibujar.

            Texture_Map    textures;                            ///< Mapa  en el que se guardan shared_ptr a las texturas cargadas.
            Sprite_List    sprites;                             ///< Lista en la que se guardan shared_ptr a los sprites creados.

            Sprite         * player;                            ///< Puntero al sprite de la lista de sprites que representa a al jugador
            Sprite         * divisor_1;                         ///< Puntero al sprite de la lista de sprites que representa a el divisor de carriles
            Sprite         * divisor_2;                         ///< Puntero al sprite de la lista de sprites que representa a el divisor de carriles
            Sprite         * divisor_3;                         ///< Puntero al sprite de la lista de sprites que representa a el divisor de carriles
            Sprite         * divisor_4;                         ///< Puntero al sprite de la lista de sprites que representa a el divisor de carriles
            Sprite         * obstacles [15] ;                   ///< Puntero al array de obstáculos
            Sprite         * bkg;                               ///< Puntero al sprite de fondo
            Sprite         * pause_button;                      ///< Puntero al botón de pausa
            Sprite         * resume_button;                     ///< Puntero al botón de continuar jugando
            Sprite         * restart_button;                    ///< Puntero al botón de reinicio
            Sprite         * help_button;                       ///< Puntero al botón de ayuda
            Sprite         * instructions_button;               ///< Puntero a la información de ayuda

            float          obstacle_speed;                      ///< Velocidad de los obstáculos
            Timer          obstacle_timer;                      ///< Cronómetro de generación de obstáculos
            float          generate_enemy_timer;                ///< Valor del contador de generación de obstáculos
            bool           pause;                               ///< Puntero a la información de ayuda

            bool           follow_target;                       ///< true si el usuario está tocando la pantalla y su player ir hacia donde toca.
            float          user_target_x;                       ///< Coordenada X hacia donde debe ir el player del usuario cuando este toca la pantalla.
            float          user_target_y;                       ///< Coordenada Y hacia donde debe ir el player del usuario cuando este toca la pantalla.

            Timer          timer;                               ///< Cronómetro usado para medir intervalos de tiempo

        public:

            /**
             * Solo inicializa los atributos que deben estar inicializados la primera vez, cuando se
             * crea la escena desde cero.
             */
            Game_Scene();

            /**
             * Este método lo llama Director para conocer la resolución virtual con la que está
             * trabajando la escena.
             * @return Tamaño en coordenadas virtuales que está usando la escena.
             */
            basics::Size2u get_view_size () override
            {
                return { canvas_width, canvas_height };
            }

            /**
             * Aquí se inicializan los atributos que deben restablecerse cada vez que se inicia la escena.
             * @return
             */
            bool initialize () override;

            /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a segundo plano.
             */
            void suspend () override;

            /**
             * Este método lo invoca Director automáticamente cuando el juego pasa a primer plano.
             */
            void resume () override;

            /**
             * Este método se invoca automáticamente una vez por fotograma cuando se acumulan
             * eventos dirigidos a la escena.
             */
            void handle (basics::Event & event) override;

            /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * actualize su estado.
             */
            void update (float time) override;

            /**
             * Este método se invoca automáticamente una vez por fotograma para que la escena
             * dibuje su contenido.
             */
            void render (Context & context) override;

        private:

            /**
             * En este método se cargan las texturas (una cada fotograma para facilitar que la
             * propia carga se pueda pausar cuando la aplicación pasa a segundo plano).
             */
            void load_textures ();

            /**
             * En este método se crean los sprites cuando termina la carga de texturas.
             */
            void create_sprites ();

            /**
             * Se llama cada vez que se debe reiniciar el juego. En concreto la primera vez y cada
             * vez que un jugador pierde.
             */
            void restart_game ();


            /**
             * Actualiza el estado del juego cuando el estado de la escena es RUNNING.
             */
            void run_simulation (float time);


            /**
             * Hace que el player  se mueva
             */
            void update_user ();

            /**
             * Dibuja la textura con el mensaje de carga mientras el estado de la escena es LOADING.
             * La textura con el mensaje se carga la primera para mostrar el mensaje cuanto antes.
             * @param canvas Referencia al Canvas con el que dibujar la textura.
             */
            void render_loading (Canvas & canvas);

            /**
             * Dibuja la escena de juego cuando el estado de la escena es RUNNING.
             * @param canvas Referencia al Canvas con el que dibujar.
             */
            void render_playfield (Canvas & canvas);

            void generate_obstacle();

            void check_obstacle_in_screen();

            void check_player_collisions();

        };

    }

#endif
